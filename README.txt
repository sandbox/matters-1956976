--  INTRODUCTIONS  --

This module implements block with translation widget using Google Translate
service.

--  INSTALLATION  --

Unpack the module into sites/all/modules and activate it
on the modules page. Then go to Admin->Structure->Blocks, select
region for block 'Google Translate' and configure it.
If you wish to control the translation of your content on the site, please
register at https://translate.google.com/manager/website/ and enter
the resulting hash code in the field 'Meta content'.
